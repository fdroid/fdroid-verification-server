
# Verification Server Bootstrap

This sets up an F-Droid
[Verification Server](https://f-droid.org/docs/Verification_Server/)
for reproducing builds of all packages in a given data repository
(e.g. _fdroiddata_).  It is used for
<https://verification.f-droid.org>.  It can also be used to run a
Verification Server on anyone else's infrastructure and/or any other
collection of fdroid build metadata files.


## Requirements

This setup assumes a base box running Debian/stretch with SSH.  It
requires at least 300GB of disk and 8GB RAM.
